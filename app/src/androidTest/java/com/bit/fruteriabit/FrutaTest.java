package com.bit.fruteriabit;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.bit.fruteriabit.daos.FrutaDao;
import com.bit.fruteriabit.database.AppDatabase;
import com.bit.fruteriabit.entities.Fruta;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class FrutaTest {
    private FrutaDao frutaDao;
    private AppDatabase appDatabase;

    @Before
    public void createDb(){
        //contexto
        Context context = ApplicationProvider.getApplicationContext();
        appDatabase = Room.inMemoryDatabaseBuilder(context, AppDatabase.class).build();
        //referenciamos DAO
        frutaDao = appDatabase.frutaDao();
        //la base está incializada
    }

    @After
    //cerramos base de datos
    public void closeDb() throws IOException{
        appDatabase.close();
    }

    @Test
    public void findbyNameTest() throws Exception{
        //creamos fruta
        Fruta fruta = new Fruta();
        fruta.setId(1);
        fruta.setNombre("naranja");

        //insertamos fruta
        frutaDao.insert(fruta);

        //busco por nombre
        Fruta buscada = frutaDao.findByNombre("naranja");
        assertTrue("No encontro la naranja que tenia", fruta.getId() == buscada.getId());

    };
}
