package com.bit.fruteriabit;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
//creamos el viewHolder
public class FrutaViewHolder extends RecyclerView.ViewHolder {
    //mostramos el nombre de la fruta
    private final TextView frutaItemView;

    //constructor
    private FrutaViewHolder(View itemView) {
        super(itemView);
        //inicializamos frutaitemView
        frutaItemView = itemView.findViewById(R.id.textViewNombre);
    }

    //creamos vista para mostrar el texto
    public void bind(String texto) {
        //bind conecta datos
        frutaItemView.setText(texto);
    }

    static FrutaViewHolder create(ViewGroup parent){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fruta_item, parent,false);
        return new FrutaViewHolder(view);
    }

}
