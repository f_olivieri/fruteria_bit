package com.bit.fruteriabit.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.bit.fruteriabit.entities.Fruta;

import java.util.List;

@Dao
public interface FrutaDao {
    //obtengo todas las frutas
    @Query("SELECT * FROM fruta")
    LiveData<List<Fruta>> getAll();

    //Insertar una fruta
    @Insert
    void insert(Fruta fruta);

    //Actualizar
    @Update
    void update(Fruta fruta);
    //Borrar
    @Delete
    void delete(Fruta fruta);

    //Encontrar fruta por nombre
    @Query("SELECT * FROM fruta where nombre like:nombre")
    Fruta findByNombre(String nombre);

    //Encontrar fruta por id
    @Query("SELECT * FROM fruta where id = :id")
    Fruta findById(int id);
}
